#include "ParallelQuickSort.h"
#include "QuickSort.h"
#include "Utils.h"
#include <chrono>
#include <iostream>
#include <sstream>

void ParallelQuickSort::sort(std::vector<int>& array, int threads, int depth) {
    auto startTime = std::chrono::high_resolution_clock::now();


    ThreadPool pool(threads);
    parallelSort(pool, array, 0, array.size() - 1, depth);

    auto endTime = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime);

    std::stringstream out;
    out
            << ANSI_COLOR_BRIGHT_YELLOW << std::setw(25) << std::right << "Parallel quick sort: " << ANSI_COLOR_RESET
            << ANSI_COLOR_GREEN << Utils::formatNumberWithSpaces(array.size()) << ANSI_COLOR_RESET
            << ANSI_COLOR_RESET << " numbers, time: "
            << ANSI_COLOR_RED << std::setw(6) << Utils::formatNumberWithSpaces(duration.count())
            << ANSI_COLOR_YELLOW << " milliseconds." << ANSI_COLOR_RESET << "\n";

    std::cout << out.str();
}

void ParallelQuickSort::parallelSort(ThreadPool& pool, std::vector<int>& array, int low, int high, int depth) {
    if (depth <= 0 || low >= high) {
        QuickSort::quicksort(array, low, high);
        return;
    }
    int pivot_index = partition(array, low, high);

    auto leftTask = [&pool, &array, low, pivot_index]() {
        parallelSort(pool, array, low, pivot_index - 1);
    };

    auto rightTask = [&pool, &array, pivot_index, high]() {
        parallelSort(pool, array, pivot_index + 1, high);
    };

    pool.enqueue(leftTask);
    pool.enqueue(rightTask);
}

int ParallelQuickSort::partition(std::vector<int>& array, int low, int high) {
    int pivot = array[high];
    int i = low - 1;

    for (int j = low; j < high; ++j) {
        if (array[j] < pivot) {
            ++i;
            std::swap(array[i], array[j]);
        }
    }

    std::swap(array[i + 1], array[high]);
    return i + 1;
}