//
// Created by Tobias Le on 01.01.2024.
//

#include "Benchmark.h"
#include <iostream>
#include <utility>
#include <vector>
#include <fstream>
#include <random>
#include <random>
#include "QuickSort.h"
#include "Utils.h"
#include "ParallelQuickSort.h"

#define ANSI_COLOR_PURPLE "\x1b[35m"
#define ANSI_COLOR_RESET "\x1b[0m"
#define ANSI_COLOR_RED "\x1b[31m"


FilenamePattern::FilenamePattern(std::string before, std::string after)
        : before(std::move(before)), after(std::move(after)) {}

std::string FilenamePattern::getFilename(int i) {
    return before + std::to_string(i) + after;
}

void generateBenchmarkData(FilenamePattern pattern, int numberCount) {
    std::random_device rd;
    std::mt19937 rng(rd());

    std::uniform_int_distribution<int> distribution(0, 2 * numberCount);

    std::string fileName = pattern.getFilename(numberCount);
    std::ofstream outputFile(fileName);
    if (outputFile.is_open()) {
        for (int i = 0; i < numberCount; ++i) {
            outputFile << distribution(rng) - numberCount << " ";
        }
        outputFile.close();
        std::cout << ANSI_COLOR_GREEN << "File '" << fileName << "' has been created." << ANSI_COLOR_RESET << std::endl;
    } else {
        throw std::runtime_error("Error creating file while generating benchmark data.");
    }
}

void benchmark(bool printSortedArray, int cores, int depth) {
    std::vector<int> rawLengths = {3000000, 4000000, 5000000, 6000000, 7000000, 8000000, 9000000, 10000000,
                                   20000000, 30000000, 40000000, 50000000, 60000000,
                                   70000000, 80000000, 90000000, 100000000};

    auto pattern = FilenamePattern("unsorted_", "_numbers.txt");

    for (int i : rawLengths) {
        std::string fileName = pattern.getFilename(i);
        std::ifstream inputFile(fileName);
        if (!inputFile.is_open()) {
            std::cout << ANSI_COLOR_PURPLE << "File '" << fileName << "' does not exist, generating benchmark data..." << ANSI_COLOR_RESET << std::endl;
            generateBenchmarkData(pattern, i);
        }
    }

    for (int i : rawLengths) {
        std::string fileName = pattern.getFilename(i);
        std::cout << std::endl << "Sorting " << ANSI_COLOR_PURPLE << fileName << ANSI_COLOR_RESET << "..." << std::endl;
        std::vector<int> unsortedArray;

        try {
            unsortedArray = Utils::parseIntVectorFromFile(fileName);
        } catch (std::runtime_error& e) {
            std::cerr << ANSI_COLOR_RED << e.what() << ANSI_COLOR_RESET << std::endl;
            throw std::runtime_error(e.what());
        }

        QuickSort::sort(unsortedArray);
        ParallelQuickSort::sort(unsortedArray, cores, depth);
    }

    if (printSortedArray) {
        std::cout << ANSI_COLOR_RED << "Print sorted array is not supported for benchmarking." << ANSI_COLOR_RESET << std::endl;
        throw std::runtime_error("Print sorted array is not supported for benchmarking.");
    }

    std::cout << ANSI_COLOR_PURPLE << "Sorting completed." << ANSI_COLOR_RESET << std::endl;
}