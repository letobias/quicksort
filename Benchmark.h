//
// Created by Tobias Le on 01.01.2024.
//

#ifndef QUICKSORT_BENCHMARK_H
#define QUICKSORT_BENCHMARK_H


#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <random>
#include <random>
#include "QuickSort.h"

#define ANSI_COLOR_PURPLE "\x1b[35m"
#define ANSI_COLOR_RESET "\x1b[0m"
#define ANSI_COLOR_RED "\x1b[31m"
#define ANSI_COLOR_GREEN "\x1b[32m"


class FilenamePattern {
public:
    FilenamePattern(std::string  before, std::string  after);

    std::string getFilename(int i);

private:
    std::string before;
    std::string after;
};

std::vector<int> parseIntVectorFromFile(const std::string& fileName);

void generateBenchmarkData(FilenamePattern pattern, int numberCount);

void benchmark(bool printSortedArray, int cores, int depth);

#endif //QUICKSORT_BENCHMARK_H
