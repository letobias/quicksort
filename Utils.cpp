//
// Created by Tobias Le on 01.01.2024.
//

#include <sstream>
#include "Utils.h"
#include <regex>

std::vector<int> Utils::parseIntVectorFromFile(const std::string& fileName) {
    std::ifstream inputFile(fileName);
    if (!inputFile.is_open()) {
        throw std::runtime_error("Error opening file " + fileName + " while parsing int vector.");
    }

    std::vector<int> unsortedArray;
    int num;
    while (inputFile >> num) {
        unsortedArray.push_back(num);
    }
    inputFile.close();

    return unsortedArray;
}

 void Utils::printArray(const std::vector<int> &array, bool sorted, std::string label) {
    auto color = sorted ? ANSI_COLOR_ORANGE : ANSI_COLOR_PINK;

    std::stringstream out;

    int maxWidth = getMaxLength(array);

    // +1 FOR PIPE ON LEFT SIDE, +2 FOR SPACE LEFT AND RIGHT
    int columnWidth = maxWidth + 3;
    int elementsPerLine = 80 / columnWidth;

    std::string sortedStr = sorted ? "sorted" : "unsorted";

     std::cout << ANSI_COLOR_PURPLE << "Printing " << sortedStr << " array..." << ANSI_COLOR_RESET << std::endl;
     out << ANSI_COLOR_CYAN << label << ANSI_COLOR_RESET << std::endl;
     auto printHyphens = [&]() -> void {
        out << std::setfill('-');

        for (size_t j = 0; j < elementsPerLine; ++j) {
            out << std::setw(columnWidth) << "";
        }

        // +1 for PIPE ON RIGHT
        out << "-" << std::endl;
        out << std::setfill(' ');
    };

    printHyphens();

    // Print numbers
    int index = 0;
    for (int i = 0; i < array.size(); i++) {
        if (i > 0 && i % elementsPerLine == 0) {
            out << "|" << std::endl;
        }
        out << "| " << color << std::setw(maxWidth) << std::right << array[i] << " " << ANSI_COLOR_RESET;
        index = i;
    }

    // Fill remaining space with empty columns
    for (int i = index + 1; i % elementsPerLine != 0; i++) {
        out << "| " << std::setw(maxWidth) << std::right << "-" << " ";
    }

    out << "|" << std::endl;


    printHyphens();

    std::cout << out.str() << std::endl;
}

void Utils::printArray(const std::vector<int>& array, bool sorted) {
    printArray(array, sorted, "");
}

void Utils::printArray(const std::vector<int> &array) {
    printArray(array, true);
}

size_t Utils::getMaxLength(const std::vector<int> &numbers) {
    std::regex colorRegex("\x1b\\[[0-9;]*m"); // Regex to remove ANSI escape codes
    size_t maxWidth = 0;

    for (int num : numbers) {
        // Remove ANSI escape codes before calculating length
        std::string numStr = std::regex_replace(std::to_string(num), colorRegex, "");
        int width = numStr.length();

        if (width > maxWidth) {
            maxWidth = width;
        }
    }

    return maxWidth;
}

std::string Utils::formatNumberWithSpaces(int number) {
    std::string numberStr = std::to_string(number);

    // Calculate the position to insert spaces
    int position = numberStr.length() % 3;

    // Iterate through the string and insert spaces
    for (int i = position; i < numberStr.length(); i += 4) {
        numberStr.insert(i, " ");
    }

    return numberStr;
}