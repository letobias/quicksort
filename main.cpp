#include <iostream>
#include <algorithm> // for std::find
#include <random>
#include "Benchmark.h"
#include "Utils.h"
#include "ParallelQuickSort.h"

#define ANSI_COLOR_PURPLE "\x1b[35m"
#define ANSI_COLOR_RESET "\x1b[0m"
#define ANSI_COLOR_RED "\x1b[31m"

/**
 * Prints the help message.
 */
void printHelp() {
    std::cout << "Usage: ./your_program [-p] [-c] [-psa] [-f filename]\n"
              << "\n"
              << "Options:\n"
              << "  -p                 Enable parallel quicksort (default: sequential)\n"
              << "  -c                 Compare sequential and parallel quicksort\n"
              << "  -psa               Print sorted array before and after sorting (not compatible with benchmark)\n"
              << "  -f filename        Specify the input file for quicksort (running benchmark if not specified)\n"
              << "  -h, --help         Display this help message\n"
              << "  -d depth           Specify the depth of the parallel quicksort (default: log2(cores))\n";
}

/**
 * Checks if the flags specified by the user are valid.
 * @param argc
 * @param argv
 */
void checkFlagValidity(int argc, char* argv[]) {
    // Check for the presence of invalid flags
    std::vector<std::string> validFlags = {"-p", "-c", "-psa", "-f", "-h", "--help", "-d"};
    for (int i = 1; i < argc; ++i) {
        if (std::find(validFlags.begin(), validFlags.end(), argv[i]) == validFlags.end()) {
            throw std::runtime_error("Invalid flag '" + std::string(argv[i]) + "'. Please use -h or --help for help.");
        }
    }
}

/**
 * Handles the -d flag.
 * @param argc
 * @param argv
 * @param cores
 * @return
 */
int handleDepth(int argc, char* argv[], int cores) {
    // Find the index of the -d flag in command-line arguments
    auto it2 = std::find(argv, argv + argc, std::string("-d"));
    bool depthSpecified = it2 != (argv + argc);
    int indexOfD = depthSpecified ? static_cast<int>(it2 - argv) : -1;
    int depth;
    if (depthSpecified && indexOfD == argc - 1) {
        throw std::runtime_error("No depth specified after -d flag.");
    } else if (depthSpecified) {
        std::string depthStr = argv[indexOfD + 1];
        depth = std::stoi(depthStr);
        std::cout << ANSI_COLOR_PURPLE << "User specified depth '" << depth << "'." << ANSI_COLOR_RESET << std::endl;
        return depth;
    } else {
        depth = log2(cores);
        std::cout << ANSI_COLOR_PURPLE << "User did not specify depth, using default depth of " << depth << ANSI_COLOR_RESET << std::endl;
        return depth;
    }
}

/**
 * Handles the the sorting of the user-specified file.
 * @param printSortedArray
 * @param compare
 * @param parallel
 * @param cores
 * @param depth
 * @param fileName
 */
void handleUserInput(bool printSortedArray, bool compare, bool parallel, int cores, int depth, const std::string& fileName) {
    std::vector<int> unsortedArray = Utils::parseIntVectorFromFile(fileName);
    std::cout << ANSI_COLOR_PURPLE << "Parsed file '" << fileName << "'." << ANSI_COLOR_RESET << std::endl;

    if (printSortedArray) {
        Utils::printArray(unsortedArray, false);
    }

    std::cout << ANSI_COLOR_PURPLE << "Running quicksort on " << fileName << ANSI_COLOR_RESET
              << std::endl;

    if (compare) {
        std::vector parallelSortedArray = unsortedArray;
        QuickSort::sort(unsortedArray);
        ParallelQuickSort::sort(parallelSortedArray, cores, depth);
        if (printSortedArray) {
            Utils::printArray(unsortedArray, true, "Normal quick sort");
            std::cout << std::endl;
            Utils::printArray(parallelSortedArray, true, "Parallel quick sort");
        }
    }
    else if (parallel) {
        ParallelQuickSort::sort(unsortedArray, cores, depth);
        if (printSortedArray) {
            Utils::printArray(unsortedArray, true);
        }
    }
    else {
        QuickSort::sort(unsortedArray);
        if (printSortedArray) {
            Utils::printArray(unsortedArray, true);
        }
    }

    std::cout << ANSI_COLOR_PURPLE << "Sorting finished." << ANSI_COLOR_RESET << std::endl;
}

/**
 * Handles the -f flag.
 * @param argc
 * @param argv
 * @return
 */
std::pair<bool, std::string> handleUserFile(int argc, char **argv) {
    auto it = std::find(argv, argv + argc, std::string("-f"));
    bool userSpecifiedFile = it != (argv + argc);
    int indexOfF = userSpecifiedFile ? static_cast<int>(it - argv) : -1;

    if (userSpecifiedFile && indexOfF == argc - 1) {
        throw std::runtime_error("No file specified after -f flag.");
    }

    std::string fileName = argv[indexOfF + 1];
    std::cout << ANSI_COLOR_PURPLE << "User specified file '" << fileName << "'." << ANSI_COLOR_RESET
              << std::endl;

    return std::make_pair(userSpecifiedFile, fileName);
}

int main(int argc, char* argv[]) {
    bool showHelp = std::find(argv, argv + argc, std::string("-h")) != (argv + argc) ||
                    std::find(argv, argv + argc, std::string("--help")) != (argv + argc);

    if (showHelp) {
        printHelp();
        return 0;
    }

    try {
        checkFlagValidity(argc, argv);
    } catch (std::runtime_error& e) {
        std::cerr << ANSI_COLOR_RED << e.what() << ANSI_COLOR_RESET << std::endl;
        return 1;
    }

    std::cout << ANSI_COLOR_PURPLE << "Program has started..." << ANSI_COLOR_RESET << std::endl;

    // Check for the -psa flag in command-line arguments
    bool printSortedArray = std::find(argv, argv + argc, std::string("-psa")) != (argv + argc);

    // Find the index of the -f flag in command-line arguments
    std::string fileName;
    bool userSpecifiedFile;
    try {
        userSpecifiedFile = handleUserFile(argc, argv).first;
        fileName = handleUserFile(argc, argv).second;
    } catch (std::runtime_error& e) {
        std::cerr << ANSI_COLOR_RED << e.what() << ANSI_COLOR_RESET << std::endl;
        return 1;
    }

    // Check if the -p flag was specified
    bool parallel = std::find(argv, argv + argc, std::string("-p")) != (argv + argc);
    int cores = std::thread::hardware_concurrency();
    int depth = handleDepth(argc, argv, cores);

    // Check if the -c flag was specified
    bool compare = std::find(argv, argv + argc, std::string("-c")) != (argv + argc);

    if (parallel || !userSpecifiedFile) {
        if (cores == 0) {
            std::cerr << ANSI_COLOR_RED << "Could not detect number of cores, using 1 core." << ANSI_COLOR_RESET
                      << std::endl;
            cores = 1;
        } else {
            std::cout << ANSI_COLOR_PURPLE << "Discovered " << cores << " cores to use" << ANSI_COLOR_RESET << std::endl;
        }

        std::cout << std::endl;
    }

    if (!userSpecifiedFile) {
        // -f flag not specified, generate benchmark data and run benchmark
        std::cout << ANSI_COLOR_PURPLE << "User has not specified a file, running a benchmark..." << ANSI_COLOR_RESET << std::endl;
        try {
            benchmark(printSortedArray, cores, depth);
        } catch (std::runtime_error& e) {
            std::cerr << ANSI_COLOR_RED << e.what() << ANSI_COLOR_RESET << std::endl;
            return 1;
        }
    } else {
        try {
            handleUserInput(printSortedArray, compare, parallel, cores, depth, fileName);
        } catch (std::runtime_error& e) {
            std::cerr << ANSI_COLOR_RED << e.what() << ANSI_COLOR_RESET << std::endl;
            return 1;
        }
    }

    return 0;
}

