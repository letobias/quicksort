//
// Created by Tobias Le on 01.01.2024.
//

#include "QuickSort.h"
#include "Utils.h"
#include <chrono>
#include <iostream>
#include <sstream>

void QuickSort::sort(std::vector<int>& array) {
    auto startTime = std::chrono::high_resolution_clock::now();
    std::stringstream out;

    int size = array.size();
    quicksort(array, 0, size - 1);

    auto endTime = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime);

    // Print in colored text
    out
    << ANSI_COLOR_BLUE << std::setw(25) << std::right << "Normal quick sort: " << ANSI_COLOR_RESET
    << ANSI_COLOR_GREEN << Utils::formatNumberWithSpaces(array.size()) << ANSI_COLOR_RESET
    << ANSI_COLOR_RESET << " numbers, time: "
    << ANSI_COLOR_RED << std::setw(6) << Utils::formatNumberWithSpaces(duration.count())
    << ANSI_COLOR_YELLOW << " milliseconds." << ANSI_COLOR_RESET << "\n";

    std::cout << out.str();
}

void QuickSort::quicksort(std::vector<int>& array, int low, int high) {
    if (low < high) {
        int pivot_index = partition(array, low, high);

        // Recursively sort the sub-arrays
        quicksort(array, low, pivot_index - 1);
        quicksort(array, pivot_index + 1, high);
    }
}

int QuickSort::partition(std::vector<int>& array, int low, int high) {
    int pivot = array[high];
    int i = low - 1;

    for (int j = low; j < high; ++j) {
        if (array[j] < pivot) {
            ++i;
            // Swap array[i] and array[j]
            std::swap(array[i], array[j]);
        }
    }

    // Swap array[i+1] and array[high] (put pivot element in its correct position)
    std::swap(array[i + 1], array[high]);

    return i + 1;
}


