//
// Created by Tobias Le on 01.01.2024.
//

#ifndef QUICKSORT_UTILS_H
#define QUICKSORT_UTILS_H

#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>

#define ANSI_COLOR_PINK "\x1b[38;2;255;182;193m"
#define ANSI_COLOR_ORANGE "\x1b[38;2;255;165;0m"
#define ANSI_COLOR_PURPLE "\x1b[35m"
#define ANSI_COLOR_CYAN "\x1b[36m"
#define ANSI_COLOR_YELLOW "\x1b[33m"
#define ANSI_COLOR_GREEN "\x1b[32m"
#define ANSI_COLOR_RESET "\x1b[0m"

class Utils {
private:
    static size_t getMaxLength(const std::vector<int>& numbers);
public:
    /**
     * Parses a file with integers separated by spaces into a vector.
     * @param fileName
     * @return
     */
    static std::vector<int> parseIntVectorFromFile(const std::string& fileName);
    /**
     * Prints the given array.
     * @param array
     * @param sorted
     * @param label
     */
    static void printArray(const std::vector<int> &array, bool sorted, std::string label);
    /**
     * Prints the given array.
     * @param array
     * @param sorted
     */
    static void printArray(const std::vector<int> &array, bool sorted);
    /**
     * Prints the given array.
     * @param array
     */
    static void printArray(const std::vector<int> &array);
    /**
     * Formats a number to have spaces between every 3 digits.
     * @param number
     * @return formatted number
     */
    static std::string formatNumberWithSpaces(int number);
};


#endif //QUICKSORT_UTILS_H