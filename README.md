# Quicksort and Parallel Quicksort Algorithm
Status: **Finished** | Version: **1.0.0** | Date: **1-1-2024** | Author: **Tobias Le**

## Overview

This repository contains implementations of the Quicksort algorithm and its parallelized version in C++. 
The goal is to compare the performance of these algorithms on various devices.

## Contents

- `QuickSort.h` and `QuickSort.cpp`: Implementation of the traditional Quicksort algorithm.
- `ParallelQuickSort.h` and `ParallelQuickSort.cpp`: Implementation of the parallelized Quicksort algorithm using a Thread Pool.
- `Benchmark.h` and `Benchmark.cpp`: Benchmarking class to measure the performance of the algorithms.
- `Utils.h` and `Utils.cpp`: Utility functions to print arrays and generate numbers.
- `Threadpool.h` and `Threadpool.cpp`: Implementation of a Thread Pool.
- `CMakeLists.txt`: CMake file to build the project.
- `main.cpp`: Main program to run the algorithms.

## Building and Running

To build the project, use the provided CMakeLists.txt file. You can specify various options when running the program:

- `-p`: Enable parallel Quicksort.
- `-c`: Compare sequential and parallel Quicksort.
- `-psa`: Print sorted array before and after sorting.
- `-f filename`: Specify the input file for Quicksort.
- `-h` or `--help`: Display help message.

## Usage

```bash
./quicksort [-p] [-c] [-psa] [-f filename]
