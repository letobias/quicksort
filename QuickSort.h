//
// Created by Tobias Le on 01.01.2024.
//

#ifndef QUICKSORT_QUICKSORT_H
#define QUICKSORT_QUICKSORT_H


#include <vector>

#define ANSI_COLOR_BLUE "\x1b[34m"
#define ANSI_COLOR_GREEN "\x1b[32m"
#define ANSI_COLOR_YELLOW "\x1b[33m"
#define ANSI_COLOR_RESET "\x1b[0m"
#define ANSI_COLOR_RED "\x1b[31m"

class QuickSort {
public:
    /**
     * Sorts the given array using the quick sort algorithm and handles printing.
     * @param array
     */
    static void sort(std::vector<int>& array);
    /**
     * Sorts the given array using the quick sort algorithm.
     * @param array
     * @param low
     * @param high
     */
    static void quicksort(std::vector<int>& array, int low, int high);
private:
    static int partition(std::vector<int>& array, int low, int high);
};


#endif //QUICKSORT_QUICKSORT_H
