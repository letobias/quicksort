//
// Created by Tobias Le on 01.01.2024.
//

#ifndef QUICKSORT_PARALLELQUICKSORT_H
#define QUICKSORT_PARALLELQUICKSORT_H

#include <vector>
#include <thread>
#include "ThreadPool.h"

#define ANSI_COLOR_BRIGHT_YELLOW "\x1b[93m"
#define ANSI_COLOR_GREEN "\x1b[32m"
#define ANSI_COLOR_YELLOW "\x1b[33m"
#define ANSI_COLOR_RESET "\x1b[0m"
#define ANSI_COLOR_RED "\x1b[31m"

class ParallelQuickSort {
public:
    /**
     * Sorts the given array using the parallel quick sort algorithm and handles printing.
     * @param array
     * @param threads
     * @param depth
     */
    static void sort(std::vector<int>& array, int threads, int depth);
private:
    static int partition(std::vector<int>& array, int low, int high);
    static void parallelSort(ThreadPool& pool, std::vector<int>& array, int low, int high, int depth = 8);
};


#endif //QUICKSORT_PARALLELQUICKSORT_H
